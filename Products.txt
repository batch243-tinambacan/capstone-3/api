{
    "productName" : "Coco Mademoiselle",
    "description" : "Ambery fragrance with a distinct character.",
    "price" : 9000,
    "stocks" : 300
}

{
    "productName" : "Allure",
    "description" : "A timeless, floral-fresh-ambery fragrance",
    "price" : 9000,
    "stocks" : 300
}

{
    "productName" : "Chance",
    "description" : "Unexpected and floral.",
    "price" : 9000,
    "stocks" : 300
}

{
    "productName" : "Gabrielle",
    "description" : "Solar and radiant with a warm trail.",
    "price" : 9000,
    "stocks" : 300
}