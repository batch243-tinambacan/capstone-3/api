const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

				productName: { 
					type: String, 
					required: [ true, "Course is required"]
				}, 

				description:{ 
					type: String, 
					required: [ true, "Course is required"]
				}, 

				price:{
					type: Number,
					required: [true, "Price is required"]
				},

				stocks:{
					type: Number,
					required: [true, "Slot is required"]
				},

				isActive: {
					type:Boolean, 
					default:true
				}, 

				createdOn: {
					type:Date,
					default: new Date() 
				}, 

				orders: [{ 
					userId: {
						type: String,
						required: [true, "UserId is required"]
					},
					orderDate: {
						type:Date,
						default: new Date() 
					}, 

				}]
		}); 

module.exports = mongoose.model("Products", productSchema)
