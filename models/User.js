const mongoose = require("mongoose");

// Define our schema
const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	cart : [{
		products : [{
			productId : String,
			productName: String,
			price: Number,
			addedOn : {
				type : Date,
				default : new Date()
			}
		}],
		quantity: Number,
			totalAmount : Number
	}],

	orders : [{
		products : [{
			productName: String,
			price: Number,
			quantity: Number
		}],
		totalAmount : Number,
		discount: Number,
		grandTotal: Number,
		purchasedOn : {
			type : Date,
			default : new Date()
		}
	}]
})

module.exports = mongoose.model("User", userSchema);

/* module.exports 
	-we can export functions, objects, and their references from one file and can use them in other files by importing them by require() method.

	-for ex, check the js files from controller folder, we can see that we accessed the schema by requiring it:
		const User = require("../models/User");
		const Product = require("../models/Product");

	- notice also that we use module.exports in each controller method (ie: module.exports.addProduct) because we need to accessed them for routing.
*/