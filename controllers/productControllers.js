const Product = require("../models/Product");
const auth = require("../auth");


// --- Create Products ---//
	module.exports.addProduct = (request, response) =>{

		let token = request.headers.authorization;
		const userData = auth.decode(token);

		console.log(userData);

		let newProduct = new Product({
			productName: request.body.productName,
			description: request.body.description,
			price: request.body.price,
			stocks: request.body.stocks
		})

		if(userData.isAdmin){
			newProduct.save().then(result =>{
				console.log(result);
				response.send(true);
			}).catch(error => {
				console.log(error);
				response.send(false);
			})
		}
		else{
			response.send(false)
		}	
	}


// --- Retrieve all active products  ---//
	module.exports.getAllActive = (request, response) =>{
		return Product.find({isActive: true}, {orders: 0, createdOn: 0, isActive: 0, __v: 0})
		.then(result => {
			response.send(result);
		}).catch(err => {
			response.send(err)
		})
	}


// ---Retrieve single product---//
	module.exports.getProduct = (request, response) => {
		const productId = request.params.productId
		return Product.findById(productId).then(result => {

			let productInfo = {
				productName : result.productName,
				description : result.description,
				price : result.price,
				stocks : result.stocks
			}
			return response.send(productInfo);
		}).catch(err =>{
			response.send(err)
		})
	}
	

// --- Retrieve all product (admin only) --- //
	module.exports.getAllProducts = (request, response) =>{
		const token = request.headers.authorization;
		const userData = auth.decode(token);

		if(!userData.isAdmin){
			return response.send("Sorry, you don't have access to this page!")
		}
		else{
			return Product.find({}).then(result => response.send(result)).catch(err => {
				console.log(err);
				response.send(err)
			})
		}
	}


// --- Update product's information --- //
	module.exports.updateProduct = (request, response) => {
		const token = request.headers.authorization;
		const userData = auth.decode(token);
		console.log(userData);

		let updatedProduct = {
			productName: request.body.productName,
			description: request.body.description,
			price: request.body.price,
			stocks: request.body.stocks
		}

		const productID = request.params.productId;
		if(userData.isAdmin){
			return Product.findByIdAndUpdate(productID, updatedProduct, {new: true}).then(result=>{
				response.send(result)
			}).catch(err =>{
				response.send(err);
			})
		}
		else{
			return response.send("You do not have permission to perform this task.")
		}
	}

// --- Archive/Unarchive a product --- //
	module.exports.archiveUnarchive = (request, response) =>{
		let token = request.headers.authorization;
		let userData = auth.decode(token);
		let idToBeUpdated = request.params.productId;

		if(userData.isAdmin){
			return Product.findByIdAndUpdate(idToBeUpdated).then(result => {
				let update = {
					isActive : !result.isActive
				}
				return Product.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(result=>{
				
					if(result.isActive){
						return response.send(true)
					}
					else{
						return response.send(true)
					}
				}).catch(err =>response.send(err))
			}).catch(err => response.send(err))
		}
		else{
			return response.send(false)
		}
	}


// --- Search a product --- //
	module.exports.searchProduct = async (request, response) => {
		try {
			const search = request.body.search;
			let productData = await Product.find({"productName" : {$regex: ".*"+search+".*", $options : '$i' }}, {_id: 0, orders: 0, createdOn: 0, isActive: 0, __v: 0})

			console.log(productData)
			if(productData.length > 0) {
				return response.send(productData)
			}
			else {
				return response.send('Product not found')
			}
		
		} catch (error) {
			response.send(false);
		}
	}