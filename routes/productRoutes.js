const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");
const auth = require("../auth.js")

// Add product route
router.post("/addProduct", auth.verify, productControllers.addProduct);

// Retrieve all active products route
router.get("/allActiveProducts", productControllers.getAllActive);

//Retrieve all products (admin only)
router.get("/allProducts", auth.verify, productControllers.getAllProducts);

//Search products
router.get("/search", productControllers.searchProduct);


//**** route with params below ****//

// Retrieve a single product route
router.get("/:productId", productControllers.getProduct);

// Update product's information route
router.put("/update/:productId", auth.verify, productControllers.updateProduct);

// Archive a product route
// router.patch("/:productId/archive", auth.verify, productControllers.archiveProduct);

// Archive and unarchive a product route
router.patch("/:productId/archive", auth.verify, productControllers.archiveUnarchive);

module.exports = router;