// ------ Setup Dependencies ------ //
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

//“CORS” stands for Cross-Origin Resource Sharing. It allows you to make requests from one website to another website in the browser, which is normally prohibited by another browser policy called the Same-Origin Policy (SOP).

// ------ Require different routes ------ //
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes")

// ------ Create app using express() ------ //
const app = express();

// ------ Database Connection ------ //
mongoose.set({ strictQuery: false });
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.azu7fek.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
})

/* 
	useNewUrlParser option
	- DeprecationWarning: current URL string parser is deprecated, and will be
	removed in a future version.
	To use the new parser, pass option { useNewUrlParser: true } to MongoClient.connect.

	useUnifiedTopology option
	- removes support for several connection options that are no longer relevant with the new topology engine: autoReconnect. reconnectTries. reconnectInterval.
*/ 

// ------ Handling of errors ------ //
mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once('open',() => console.log("Now connected to MongoDB Atlas"));

//------ Middlewares ------ //
app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//------ Allow access to different routes ------ //
app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT||4000}`);
})